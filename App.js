import React from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import Forcasts from './components/forcast';
import Weather from './components/weather';

export default class App extends React.Component {
  state = {
    currentWeather: '10',
    currentWeatherMin: '4',
    currentDay: 'Sunday',
    icon: 'ios-water-outline',
    weatherForcast: [
      {
        day: 'Monday',
        temp: '20',
        tempMin: '15',
        icon: 'ios-rainy-outline'
      },
      {
        day: 'Tuesday',
        temp: '13',
        tempMin: '12',
        icon: 'ios-sunny-outline'
      },
      {
        day: 'Wednesday',
        temp: '26',
        tempMin: '20',
        icon: 'ios-partly-sunny-outline'
      }
    ]
  }
  
  render() {
    let forcastItems = this.state.weatherForcast.map((aVal, aKey) => {
      return <Forcasts key={aKey} keyval={aKey} val={aVal} changeWeatherMethod={() => this.changeWeather(aVal) }/>
    });

    return (
      <View style={styles.container}>
        <View style={styles.statusBar}>
          <StatusBar />
        </View>
        <View style={styles.currentDayWrapper}>
          <Text style={styles.currentDay}>{this.state.currentDay}</Text>
        </View>
        <View style={styles.weatherWrapper}>
          <Weather weatherTempCur={ this.state.currentWeather } currentIcon={ this.state.icon } />
        </View>
        <View style={styles.weatherForcastWrapper}>
          { forcastItems }
        </View>
      </View>
    );
  }

  changeWeather = function(aProps) {
    this.setState({
      currentWeather: aProps.temp,
      currentDay: aProps.day,
      currentWeatherMin: aProps.tempMin,
      icon: aProps.icon
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: StatusBar.currentHeight,
    backgroundColor: '#3eb1de',
  },
  currentDayWrapper: {
    backgroundColor: '#3eb1de',
    paddingBottom: 5,
    paddingTop: 5,
    alignItems: 'center',
  },
  currentDay: {
    color: '#fff',
    fontSize: 25,
  },
  weatherWrapper: {
    flex: 4,
  },
  weatherForcastWrapper: {
    flex: 1,
    flexDirection: 'row',
  }
});