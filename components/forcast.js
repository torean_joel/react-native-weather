import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default class Forcast extends React.Component {
  render() {
    return (
      <View style={styles.container} key={ this.props.keyval }>
        <TouchableOpacity style={styles.forcastItem} useForeground={true} onPress={ this.props.changeWeatherMethod }>
            <Text style={styles.forcastDay}>{ this.props.val.day }</Text>
            <Text style={styles.forcastIcon}>
                <Ionicons name={ this.props.val.icon } size={40} color='white'/>
            </Text>
            <Text style={styles.forcastTemp}>{ this.props.val.tempMin }&deg;\{ this.props.val.temp }&deg;</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3eb1de',
  },
  forcastItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 18,
  },
  forcastDay: {
    fontSize: 13,
    color: '#fff',
  },
  forcastTemp: {
    flex: 1,
    fontSize: 13,
    paddingLeft: 10,
    color: '#fff',
  },
  forcastIcon: {
    paddingTop: 8,
    paddingBottom: 8
  }
});
