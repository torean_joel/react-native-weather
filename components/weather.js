import React from 'react';
import { StyleSheet, Text, Image, View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export default class Weather extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image source={require('../assets/bg.png')} style={styles.backgroundImage} />
        <View style={styles.weatherDetails}>
          <Text style={styles.currentTemp}>
            <Text style={styles.maxTemp}>{this.props.weatherTempCur}&deg;</Text>
          </Text>
        </View>
        <View style={styles.weatherIcon}> 
          <Ionicons name={ this.props.currentIcon } size={200} color="white"/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2792bd',
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
    position: 'absolute'
  },
  weatherDetails: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  weatherIcon: {
    flex: 1,
  },
  weaterType: {
    fontSize: 30,
    color: '#fff',
  },
  currentTemp: { 
    color: '#fff',
  },
  maxTemp: {
    fontSize: 100,
  },
  minTemp: {
    fontSize: 30,
  },
  separator: {
    fontSize: 80
  }
});
